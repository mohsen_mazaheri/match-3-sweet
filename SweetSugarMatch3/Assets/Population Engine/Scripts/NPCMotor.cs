﻿/*
	POPULATION ENGINE
	------------------------------------
	Custom Script Integration sample : VERSION 2.0

	JUNE 2018 UPDATES TO THIS SCRIPT: 
	NavMesh Agent pathfinding added.
	Added simple State manager for runtime use.
	Most code have been refactored and reorganized for simplicity of use.
	All previous functionality and variable names are retained.


	NPCMotor.cs
	
	REQUIREMENTS: "NPC EVENTS" component must be attached to the NPC player
	
	DESCIRPTION: Shows how to use waypoint data retrieved from the Population Generator
	to move your actor towards the waypoints.
	It uses the NPC Events component, which the Population Generator uses to communicate with custom scripts
	such as this one.

	(C) 2014-2018 AIBotSystem.com
	Support: aibotsystem@gmail.com

	TERMS: Your usage of this script and all associated assets of this product
	means you accept the Unity Asset Store terms and conditions.
	Please know that you have purchased a LICENSE to use this product and associated scripts
	did not purchase the source code itself, including this script.
	Therefore, resale of this script or any part of this product by itself is strictly prohibited.
	Commercial usage is allowed only if you integrate this product and scripts into your gaming projects.

*/

using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using PopulationEngine;

namespace PopulationEngine{

[RequireComponent (typeof (PopulationEngine.NPCEvents))]

public class NPCMotor : MonoBehaviour {
	private Animator avatar;
	private UnityEngine.AI.NavMeshAgent agent;
	private PopulationEngine.NPCEvents npcEvents;

	[Header("NAVMESH AGENT? (Requires Baked NavMesh)")]
	public bool useNavMeshAgent = false;	
	public float navMeshRadius = 0.2f;

	[Header("ANIMATION SETTINGS")]
	public bool useAnimator = false;

	[Header("IDLE SETTINGS")]
	public float minIdleTime = 0;	// random min idle time
	public float maxIdleTime = 6;	// random max idle time
	private float idleTime = 3;		

	[Header("Random Idle Animation State Names (optional)")]
	public bool randomIdleEnabled = false;
	public string[] randomIdleAnims;
	public string randomIdleStopAnim = "Stop Idle";	
	
	private Vector3 currentDestination;
	

	[Header("MOVEMENT SETTINGS")]
	public float minSpeed = 1;
	public float maxSpeed = 3;
	private float currentSpeed = 0;
	private float originalSpeed = 0;	// holds original speed to rebound from idle

	public float minTurn = 0.1f;
	public float maxTurn = 3;	
	private float currentTurnSpeed = 6;
	public float stopDistance = 1;

	[Header("Ignore Y Position? (Uncheck for airplanes)")]
	public bool ignoreY = true;

	[Header("HEAD LOOK AND FOCUS")]
	public bool headLookActive = false; // set to true to turn head towards object
	public bool focusOnIdle = false;	// set to true to turn entire body to custom focus object on idle only
	public float focusDistance = 5;		// when to start focusing on object?
	public Transform customFocusObject;

	
	[Header("RUNTIME")]
	public Transform currentWaypoint = null;
	public int currentWaypointIndex = -1;
	public enum NavState {ARRIVED, IDLE_STOP, IDLE, CALCULATING_PATH, MOVING_TO_DESTINATION}
	public NavState navState = NavState.IDLE;

	private Transform self_transform;	// cache self for better performance

	// Use this for initialization
	void Start () {
		// setup:
		npcEvents = GetComponent<PopulationEngine.NPCEvents>();
		self_transform = transform; // cache for better performance

		// setup speeds:
		originalSpeed = Random.Range(minSpeed, maxSpeed);
		currentSpeed = 0;

		// set random turn speed:
		currentTurnSpeed = Random.Range(minTurn, maxTurn);				


		// setup animator:
		if (useAnimator){
			avatar = GetComponent<Animator>();
		}		

		// check for headlook component:
		if (headLookActive){
			if (GetComponent<HeadLookController>() == null)	{
				headLookActive = false;
			}
		}		

		// setup navmesh agent:
		if (useNavMeshAgent){
			agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

			if (agent == null){
				agent = gameObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
				agent.obstacleAvoidanceType = UnityEngine.AI.ObstacleAvoidanceType.LowQualityObstacleAvoidance;
			}

			if (agent != null){
				agent.speed = originalSpeed;
				agent.angularSpeed = currentTurnSpeed * 20;
				agent.acceleration = originalSpeed;
				agent.stoppingDistance = stopDistance;
				agent.radius = navMeshRadius;

				currentDestination = transform.position;
			}
		}

		// error fixes:
		if (currentWaypointIndex < 0){
			currentWaypointIndex = 0;
		}
	}
	
	void Update(){
		if (npcEvents.canMove){

			// states
			if (navState == NavState.ARRIVED){
				ClearDestination();
				DoIdle();
			}

			else if (navState == NavState.CALCULATING_PATH){
				CalculateNewPath();
			}

			else if (navState == NavState.MOVING_TO_DESTINATION){

				// check status
				if (HasArrived()){
					navState = NavState.ARRIVED;
				}
				
				DoMovement();
			}

			else if (navState == NavState.IDLE){
				GetNewDestination();
			}
		}

		// update currentSpeed variable, if navmesh agent:
		if (useNavMeshAgent && agent != null){
			//Debug.Log(gameObject.name + agent.velocity.z);
			currentSpeed = Mathf.Abs(agent.velocity.z);
		}

		// update animator's speed parameter so it plays the right movement animation:
		if (useAnimator && avatar != null){
			avatar.SetFloat("Speed", currentSpeed);
		}

		// update headlook:
		// GET FOCUS OBJECTS, IF NEEDED:
		if (npcEvents.focusObjects && npcEvents.objectsToFocus.Length != 0){
			FindClosestObjectToFocus();
		}

		// GET TAG TO FOCUS:
		else if (npcEvents.focusTag && npcEvents.tagToFocus != ""){
			FindClosestTagsToFocus();
		}




		// GET FOCUS OBJECTS, IF NEEDED:
		if (npcEvents.focusObjects && npcEvents.objectsToFocus.Length != 0){
			FindClosestObjectToFocus();
		}

		// GET TAG TO FOCUS:
		else if (npcEvents.focusTag && npcEvents.tagToFocus != ""){
			FindClosestTagsToFocus();
		}


		// headlook:
		if (headLookActive){
			if (customFocusObject != null){
				GetComponent<HeadLookController>().target = customFocusObject.position;
				GetComponent<HeadLookController>().effect = 1;
			}

			else {
				GetComponent<HeadLookController>().target = Vector3.zero;
				GetComponent<HeadLookController>().effect = 0;
			}	

		}	
	}




	void FindClosestObjectToFocus(){
		// loop through each object from NPCEvents component and find the closest by comparing distances:
		// REQUIRES NPC EVENTS COMPONENT attached

		float tempDistance = 9999;
		GameObject tempFocusObject = null;

		// if there's only 1 object, then grab the first index:
		if (npcEvents.objectsToFocus.Length != 0){
			foreach(GameObject x in npcEvents.objectsToFocus){
				float newTempDist = Vector3.Distance(self_transform.position, x.transform.position);
				if ((newTempDist < tempDistance) && newTempDist <= focusDistance){

					// this one is closer so we store it:
					tempDistance = newTempDist;
					tempFocusObject = x;
				}
			}
		}

		if (tempFocusObject == null){
			customFocusObject = null;
		} else {
			customFocusObject = tempFocusObject.transform;	// load the closest object up			
		}

		
	}
	

	void FindClosestTagsToFocus(){
		// get list of objects from tag, then loop through each object and find closest
		// loop through each object and find the closest by comparing distances:
		// REQUIRES NPC EVENTS COMPONENT attached

		GameObject[] listOfObjectsWithTag = GameObject.FindGameObjectsWithTag(npcEvents.tagToFocus);

		float tempDistance = 9999;
		GameObject tempFocusObject = null;

		// if there's only 1 object, then grab the first index:
		if (listOfObjectsWithTag.Length != 0){

			foreach(GameObject x in listOfObjectsWithTag){
				float newTempDist = Vector3.Distance(self_transform.position, x.transform.position);
				
				if ((newTempDist < tempDistance) && newTempDist <= focusDistance){
					// this one is closer so we store it:
					tempDistance = newTempDist;
					tempFocusObject = x;
				}
			}
		}

		if (tempFocusObject == null){
			customFocusObject = null;
		} else {
			customFocusObject = tempFocusObject.transform;	// load the closest object up			
		}
	}


	void DoMovement(){
		if (useNavMeshAgent){
			agent.SetDestination(currentDestination);		
		} else {
			// regular movement:
	        float step = currentSpeed * Time.deltaTime;
	        transform.position = Vector3.MoveTowards(self_transform.position, currentDestination, step);

	        // regular rotation:
	        RotateTowardsDest(currentDestination);
		}
	}

	// regular rotation (not used if use-NavMesh Agent is selected)
	void RotateTowardsDest(Vector3 dest){
		Vector3 targetDir = Vector3.zero;

		if (ignoreY){
			targetDir = new Vector3(dest.x, self_transform.position.y, dest.z);
		} else {
			targetDir = dest;
		}

		Quaternion rotation = Quaternion.LookRotation(targetDir - self_transform.position);
		self_transform.rotation = Quaternion.Slerp(self_transform.rotation, rotation, Time.deltaTime * currentTurnSpeed);						        

	}



	void GetNewDestination(){
		if (!IsAtEndOfSequence()){
			currentDestination = npcEvents.popGen.GetNextWaypointPosition(currentWaypointIndex);
			// for debug purposes,...also get the Transform of waypoint:
			currentWaypoint = npcEvents.popGen.GetWaypointByID(currentWaypointIndex);	

			// increment to next waypoint for next run:
			currentWaypointIndex = npcEvents.popGen.GetNextWaypointIndex(currentWaypointIndex);

			SetNewDestination(currentDestination);
		}
	}

	public void SetNewDestination(Vector3 x){
		if (useNavMeshAgent){
			agent.ResetPath();
		}

		currentDestination = x;
		CalculateNewPath();
	}

	void CalculateNewPath(){
		navState = NavState.CALCULATING_PATH;

		if (useNavMeshAgent){
			// navmesh agent GO:
			agent.SetDestination(currentDestination);

			if (agent.hasPath){
				navState = NavState.MOVING_TO_DESTINATION;
			}
		} else {
			// regular movement GO:
			currentSpeed = originalSpeed;
			navState = NavState.MOVING_TO_DESTINATION;	
		}

		
	}

	void DoIdle(){
		// Debug.Log("start idle");
		navState = NavState.IDLE_STOP;

		if (!useNavMeshAgent){
			// if standard movement, set speed to 0 to stop it:
			currentSpeed = 0;
		}

		// play random idle?
		if (randomIdleEnabled && randomIdleAnims.Length != 0 && useAnimator && avatar != null){
			avatar.CrossFade(randomIdleAnims[Random.Range(0, randomIdleAnims.Length)], 0.15f);
		}

		// look at object (focus)
		if (focusOnIdle && customFocusObject != null){
			Vector3 focusPosition = new Vector3(customFocusObject.position.x, self_transform.position.y, customFocusObject.position.z);
			self_transform.LookAt(focusPosition);
		}		

		GetRandomIdleTime();
		Invoke("StopIdle", idleTime);
	}

	void StopIdle(){
		// Debug.Log("stop idle");
		navState = NavState.IDLE;
	}

	void ClearDestination(){
		if (useNavMeshAgent){
			agent.ResetPath();
		}

		currentDestination = self_transform.position;
	}

	bool HasArrived(){
		return ((useNavMeshAgent && agent.remainingDistance <= stopDistance) || (!useNavMeshAgent && Vector3.Distance(self_transform.position, currentDestination) <= stopDistance));
	}

	bool IsAtEndOfSequence(){
		return npcEvents.popGen.DoesRouteEnd(currentWaypointIndex+1);
	}


	void GetRandomIdleTime(){
		idleTime = Random.Range(minIdleTime, maxIdleTime);
	}




}
}